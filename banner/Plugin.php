<?php namespace Airasiabig\Banner;

use System\Classes\PluginBase;
use Backend;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    public function registerPermissions()
    {
        return [
            'airasiabig.banner.userBanner' => [
                'label' => 'Manage user banner',
                'tab' => 'Manage Banner'
            ],
            'airasiabig.banner.banner' => [
                'label' => 'Manage administrator banner',
                'tab' => 'Manage Banner'
            ]
            
        ];
    }

    public function registerNavigation()
    {
        return [
            'Banner' => [
                'label'       => 'Banner',
                'condition'   => '',
                'url'         => Backend::url('airasiabig/banner/userbanner'),
                'icon'        => 'oc-icon-photo',
                'permissions' => ['airasiabig.onboarding.*'],
                'order'       => 500,

                'sideMenu' => [
                    'Admin Onboarding' => [
                        'label'       => 'Admin Banner',
                        'icon'        => 'oc-icon-photo',
                        'url'         => Backend::url('airasiabig/banner/banner'),
                       // 'permissions' => ['acme.(blog.access_posts']
                    ],
                    'User Onboarding' => [
                        'label'       => 'User Banner',
                        'icon'        => 'oc-icon-photo',
                        'url'         => Backend::url('airasiabig/banner/userbanner'),
                        //'permissions' => ['acme.blog.access_categories']
                    ]
                ]
            ]
        ];
    }
}
