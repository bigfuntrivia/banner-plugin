<?php namespace Airasiabig\Banner\Models;

use Model;

/**
 * Model
 */
class Country extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'airasiabig_country_countries';

   public $hasMany = [
        'language' => [
            'AirAsiaBig\Banner\Models\Banner',
            'table' => 'airasiabig_banner_image',
            'key' => 'country_id', // key of the B class (B.id)
            'otherKey' => 'id' // column of the A model that references B (A.model_b_id)
    ],
    ];

    public function scopeApplyLanguageFilter($query, $filtered)
    {
        return $query->whereHas('onboarding', function($q) use ($filtered) {
            $q->whereIn('id', $filtered);
        });
    }
}