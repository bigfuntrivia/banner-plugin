<?php namespace Airasiabig\Banner\Models;

use Model;
use Airasiabig\Language\Models\Language;

/**
 * Model
 */
class Banner extends Model
{
   use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
        'image1' => 'required',
        //'language_id'=> 'unique:airasiabig_banner_image:country_id',
        //'language_id' => 'exists:language_id,country_id',

    ];

    public $primaryKey = 'id';

    public $belongsTo = [
        'country' => [
            'Airasiabig\Country\Models\Country',
            //'order' => 'name desc'
        ],
        'language' => [
            'Airasiabig\Language\Models\Language',
            //'order' => 'name desc'
        ],
    ];

    
    


    public $timestamps = false;

    public $table = 'airasiabig_banner_image';

    public $attachOne = [
        'image1' => 'System\Models\File',
        'image2' => 'System\Models\File',
        'image3' => 'System\Models\File',
        'image4' => 'System\Models\File',
        'image5' => 'System\Models\File'
    ];


public function getLanguageIdOptions()
{
    return Language::getNameList();
}  

    
    public static function getNameList()
    {
        if (self::$nameList)
            return self::$nameList;

        return self::$nameList = self::lists('name', 'id');
    }

    
}