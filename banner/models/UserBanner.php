<?php namespace Airasiabig\Banner\Models;

use Model;
use Airasiabig\Country\Models\Country;

/**
 * Model
 */
class UserBanner extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
    ];

    public $primaryKey = 'country_id';

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    public $hasMany = [
        'language' => [
            'AirAsiaBig\Banner\Models\Banner',
            'table' => 'airasiabig_banner_image',
            'key' => 'country_id', // key of the B class (B.id)
            'otherKey' => 'country_id' // column of the A model that references B (A.model_b_id)
    ],
    ];

    public $hasOne = [
    'country' => [
        'AirAsiaBig\Country\Models\Country',
        'key' => 'id', // key of the B class (B.id)
        'otherKey' => 'country_id' // column of the A model that references B (A.model_b_id)
    ],
];
    /**
     * @var string The database table used by the model.
     */
    public $table = 'backend_user_countries';
}