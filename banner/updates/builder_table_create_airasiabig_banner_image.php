<?php namespace Airasiabig\Banner\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAirasiabigBannerImage extends Migration
{
    public function up()
    {
        Schema::create('airasiabig_banner_image', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('country_id');
            $table->integer('language_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('airasiabig_banner_image');
    }
}
