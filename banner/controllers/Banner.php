<?php namespace Airasiabig\Banner\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use BackendAuth;
use Backend\Classes\BackendController;

class Banner extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController','Backend\Behaviors\ReorderController'];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = ['airasiabig.banner.banner'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Airasiabig.Banner', 'main-menu-item');
    }
}