<?php namespace Airasiabig\Banner\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use BackendAuth;

class UserBanner extends Controller
{
    public $implement = ['Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend.Behaviors.RelationController',
        'Backend\Behaviors\ReorderController'
        ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = ['airasiabig.banner.userBanner'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Airasiabig.Onboarding', 'main-menu-item', 'side-menu-item2');
    }

    public function index()
    {
        $this->bodyClass = 'slim-container';
        $this->makeLists();
    }

    public function listExtendQuery($query, $definition = null){
        $user = BackendAuth::getUser();
        {
            $uID = $user->id;
            //echo $uID;
            $query->where('user_id', '=', $user->id);
        }
    }
}